# ASC-Programacion


# Primeros Pasos

> Los links no figuran aún, sólo maquetado. El único que funciona es Teoría->02

* Instalar un editor de texto. Recomendado: Visual Studio Code. [Link al pagina del repo con la explicación de VSCode y la página de descarga oficial]()

* GitHub [Link a la pagina del repo con la explicación de GitHub]()

* Git [Link a la pagina  del repo con la explicación de Git]()

* Node [Link a la pagina del repo con la explicación de NodeJs]()


## Teoría (cada item con el link a la página indicada)

01. GIT
02. Introducción a Javascript: Variables, tipos de datos y funciones [Ir a la teoría](https://gitlab.com/trucchienzo/asc-programacion/-/blob/main/teoria/README.md)
03. Flujos de control, operadores de comparación, bucles for y Array
04. Objetos
05. Clases y prototype
06. Callbacks

### Extras
01. HTML
02. CSS


