
#### Formato de cada página
> Título del página. En esta página se incluiría todo lo relacionado al item "Teoría -> 02. Introd. a JS: Variables, tipos de datos y funciones"

#### Introducción a Javascript: Variables, tipos de datos y funciones
> Texto de explicación rápida: 

Una variable es una forma de almacenar el valor de algo para usar más tarde. 

Para crear una variable en JavaScript utilizamos la palabra clave var, seguida de un espacio y el nombre de la variable (con este nombre podremos hacer referencia a ella luego). Además de declarar una variable, podemos asignarle un valor usando el signo = .

Existen tres formas de declarar una variable:

```bash
    var nombre = 'Tony';
    let apellido = 'Stark';
    const comidafavorita = 'Milanesas';
```


##### Tipos de variables
##### Tipos de datos
##### Tipos de operadores
##### Objetos Globales y métodos
##### Introducción a las Funciones (estructura, argumentos, return, alcance)
##### Control de flujo y operadores de comparación
